import discord
from discord.ext import commands

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(command_prefix = "!", description = "Bot de la Cyberpromo lol", intents=intents)

@bot.event
async def on_ready():
    print('Connecté en tant que :')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.event
async def on_member_join(member):
    guild = member.guild
    if guild.system_channel is not None:
        to_send = 'Bienvenue {0.mention} ! Et bien sûr :'.format(member, guild)
        await guild.system_channel.send(to_send, file=discord.File('pics/presente_toi.gif'))

@bot.command()
async def tg(ctx):
    f = discord.File('pics/tg1.png')
    await ctx.send(file=f)

@bot.command()
async def IS(ctx):
    f = discord.File('pics/is.png')
    await ctx.send(file=f)

@bot.command()
async def supprime(ctx):
    f = discord.File('pics/supprime.png')
    await ctx.send(file=f)

bot.run("TOKEN")
